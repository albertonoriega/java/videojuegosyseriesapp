
package videojuegosyseriesapp;


public class Videojuego {
    
    public String titulo;
    public int horasEstimadas=10;
    public boolean prestado=false;
    public String genero;
    public String compania;

    public Videojuego() {
    }

    public Videojuego(String titulo, int horasEstimadas) {
        this.titulo = titulo;
        this.horasEstimadas =horasEstimadas;
    }

    public Videojuego(String titulo, int horasEstimadas, String genero, String compania) {
        this.titulo = titulo;
        this.horasEstimadas = horasEstimadas;
        this.genero = genero;
        this.compania = compania;
    }

    public String getTitulo() {
        return titulo;
    }

    public int getHorasEstimadas() {
        return horasEstimadas;
    }

    public String getGenero() {
        return genero;
    }

    public String getCompañia() {
        return compania;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setHorasEstimadas(int horasEstimadas) {
        this.horasEstimadas = horasEstimadas;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public void setCompañia(String compania) {
        this.compania = compania;
    }

    @Override
    public String toString() {
        return "Título del videojuego:" + titulo + " , horas estimadas de juego:" + horasEstimadas + " , prestado" + prestado + " , genero:" + genero + " , compañia:" + compania + '}';
    }
    
    public void entregar() {
        this.prestado=true;
    }
    
    public void devolver() {
        this.prestado=false;
    }
    
    public String isEntregado(){
        if(this.prestado=true){
            return "Prestado";
        } else {
            return "Entregado";
        }
    }
    
    public boolean compareTo(Videojuego a) {
        if (this.horasEstimadas>a.horasEstimadas){
            return true; // Devuelve true cuando las temporadas 
        } else  {
            return false;
        }
         
    }

    
}
