
package videojuegosyseriesapp;

public class Serie {
     public String titulo;
   public int temporadas=3;
   public boolean prestado=false;
   public String genero;
   public String creador;

    public Serie() {
        
    }

    public Serie(String titulo, String creador) {
        this.titulo = titulo;
        this.creador = creador;
    }

    public Serie(String titulo, int temporadas, String genero, String creador) {
        this.titulo = titulo;
        this.temporadas = temporadas;
        this.genero = genero;
        this.creador = creador;
    }

    public String getTitulo() {
        return titulo;
    }

    public int getTemporadas() {
        return temporadas;
    }

    

    public String getGenero() {
        return genero;
    }

    public String getCreador() {
        return creador;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setTemporadas(int temporadas) {
        this.temporadas = temporadas;
    }

   
    public void setGenero(String genero) {
        this.genero = genero;
    }

    public void setCreador(String creador) {
        this.creador = creador;
    }

    @Override
    public String toString() {
        return "Titulo de la serie:" + titulo + ", número de temporadas:" + temporadas + " , prestado:" + prestado + " , genero:" + genero + ", creador:" + creador + '}';
    }
    
    public void entregar() {
        this.prestado=true;
    }
    
    public void devolver() {
        this.prestado=false;
    }
    
    public boolean isEntregado(){
        if(this.prestado==true){
            return true;
        } else {
            return false;
        }
    }
    
    public boolean compareTo(Serie a) {
        if (this.temporadas>a.temporadas){
            return true; // Devuelve true cuando las temporadas 
        } else  {
            return false;
        }
         
    }

    
    
}
