
package videojuegosyseriesapp;


public class VideojuegosySeriesApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // Creamos dos objetos de la clase series
        Serie  a, b ;
        
        a = new Serie("fewfe", 7, "gfegre", "fewfge");
                
        b = new Serie("fewfe", 5, "gfegre", "fewfge");
        // Probamos el metodo compareTo      
        System.out.println(b.compareTo(a));
        System.out.println(a.compareTo(b));
        
        
        Serie series[]= new Serie[5]; // series es un array de objetos de la clase serie
        
        series[0] = new Serie("Game of thrones", 11, "Ficción", "George R. R. Martin");
        series[1] = new Serie("The walking dead", 8, "Zombies", "Robert Kirkman");
        series[2] = new Serie("El juego del calamar", 1, "Koreana", "Hwang Dong-hyuk");
        series[3] = new Serie("Breaking Bad", 9, "Quimica", "Vince Gilligan");
        series[4] = new Serie("Sons of anarchy", 13, "motos", "Kurt Sutter");
        // acceder al titulo del objeto situado en la primera posicion del array
        System.out.println(series[0].getTitulo());
        
        // Bucle que te imprime el titulo y las temporadas de todos los registros
        for (int i=0;i<series.length;i++){
            System.out.println("Titulo de la serie: "+ series[i].getTitulo() +" Temporadas: " + series[i].getTemporadas());
            
        }
        
        
        // Mostramos la propiedad prestado del objeto a
        System.out.println(a.isEntregado());
        // Usamos el metodo entrgar al metodo a (camabiamos de entregado a prestado)
        a.entregar();
        // Comprobamos que ha cambiado
        System.out.println(a.isEntregado());
        // Cambiamos la propiedad prestado de la posicion 0 y la posicion 2 del array de series
        series[0].entregar();
        series[2].entregar();
        
        // Bucle que muestra los objetos cuya propiedad prestado = false
        int contador=0; // declaramos la variable contador para contabilizar el numero de objetos que estan entregados
            for (int i=0;i<series.length;i++) {
        
                if (series[i].isEntregado()==false) {
                    contador++; // contador = contador + 1
                    System.out.println(series[i]);
                }
            
            }
        // Imprimimos el numero de objetos que estan entregados
        System.out.println(contador);
        
        
        Serie serieMayor=series[0]; // Creamos una variable de tipo objeto que tomamos la posicion 0 como serie con mas temporadas
        for (int i=1;i<(series.length);i++){
            
            if(series[i].compareTo(serieMayor)==true){      
              // Si la serie[i] tiene mas temporadas que la serie mayor, cambiamos la serieMayor
                serieMayor=series[i];
            }
            
            }
        // Imprimimos la serieMayor
        System.out.println(serieMayor);

        

        } 
    }
    

